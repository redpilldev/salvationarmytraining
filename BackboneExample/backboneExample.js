// This example shows how to instantiate a view and just append some text
var HelloWorldView = Backbone.View.extend({
	el: '#helloWorld',
	initialize: function() {
		this.render();
	},
	render: function() {
		this.$el.html("Hello World");
	}
});

// Start the helloWorld example
function startHelloWorld() {
	var helloWorld = new HelloWorldView();
}

var InjectAModel = Backbone.Model.extend({
	defaults: {
		exampleValue: 'Hello Universe'
	}
});

var InjectAView = Backbone.View.extend({
	tagName: 'div',
	className: 'well well-sm text-center',
	template: _.template('<h4><%= exampleValue %></h4>'),
	initialize: function() {
		this.model = new InjectAModel();
	},
	render: function() {
		this.$el.html(this.template(this.model.attributes));
		$('#injectAView').html(this.el);
	}
});

// Let's use a collection and build the left side navigation
var menuItems = [
	{
		title: 'What is a View',
		linkUrl: '../BackboneExample/BackboneView.html'
	},
	{
		title: 'What is a Template',
		linkUrl: '../BackboneExample/BackboneTemplate.html'
	},
	{
		title: 'What is a Model',
		linkUrl: '../BackboneExample/BackboneModel.html'
	},
	{
		title: 'What is a Collection',
		linkUrl: '../BackboneExample/BackboneCollection.html'
	},
	{
		title: 'What is a Router',
		linkUrl: '../BackboneExample/BackboneRouter.html'
	}
];

var menuCollection = new Backbone.Collection(menuItems);

var MenuItemView = Backbone.View.extend({
	tagName: 'li',
	template: _.template('<a href=\'<%= linkUrl %>\'><%= title %></a>'),
	initialize: function(options) {
		this.model = options.model;
		this.render();
	},
	render: function() {
		// Check if the model has 'currentPage' in the linkUrl, if so add the active class
		var currentPath = location.pathname;
		var currentPage = currentPath.substring(currentPath.lastIndexOf('/'));
		if (this.model.get('linkUrl').indexOf(currentPage) > -1) {
			this.$el.addClass('active');
		}
		this.$el.html(this.template(this.model.attributes));
	}
});

var MenuView = Backbone.View.extend({
	el: '#leftMenu',
	initialize: function(options) {
		this.collection = menuCollection;
		this.render();
	},
	render: function() {
		var that = this;
		// Loop thru all the items in the collection and render a MenuItem. 'menuItem' is the model in the collection
		this.collection.forEach(function(menuItem) {
			var menuItemView = new MenuItemView({model: menuItem});
			that.$el.append(menuItemView.el);
		});
	}
});

function startBackboneCollection() {
	var menuView = new MenuView();
}

var WeatherModel = Backbone.Model.extend({
	idAttribute: 'dt',
	defaults: {
		temp: {
			max: 0,
			min: 0
		},
		weather: [{
			description: '',
			icon: ''
		}],
		dt: new Date().toLocaleDateString(),
		humidity: 0,
		speed: 0
	}
});

var DailyWeatherCollection = Backbone.Collection.extend({
	url: 'http://api.openweathermap.org/data/2.5/forecast/daily',
	model: WeatherModel,
	parse: function(response) {
		return response.list;
	}
});

var WeatherItemView = Backbone.View.extend({
	tagName: 'div',
	className: 'panel panel-info',
	initialize: function(options) {
		this.model = options.model;
	},
	render: function() {
		var tmpl = _.template($('#weatherItemTemplate').html());
		this.$el.html(tmpl(this.model.attributes));
	}
});

var WeatherItemsView = Backbone.View.extend({
	el: '#weatherItems',
	initialize: function(options) {
		this.collection = new DailyWeatherCollection();
	},
	render: function() {
		var that = this;
		this.collection.fetch({
			data: {
				q: 'Atlanta',
				units: 'imperial',
				cnt: 5
			},
			success: function(response, statusText, xhr) {
				response.forEach(function(weatherItem) {
					var weatherItemView = new WeatherItemView({model: weatherItem});
					weatherItemView.render();
					that.$el.append(weatherItemView.el);
				});
			}
		});
	}
});
