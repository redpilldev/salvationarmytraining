// Contact Model
var Contact = Backbone.Model.extend({
	idAttribute: '@unid',
	url: function() {
		if (this.isNew()) {
			return 'api/data/documents?Form=contact';
		}else{
			return Backbone.Model.prototype.url.apply(this);
		}
	},
	isDirty: false,
	urlRoot: 'api/data/documents/unid',
	defaults: {
		FirstName: '',
		LastName: '',
		Email: '',
		HomePhone: '',
		CellPhone: '',
		Supervisor: ''
	},
	initialize: function() {
		var that = this;
		this.on({
			'request': function() {
				that.isDirty = false;
			},
			'sync': function() {
				that.isDirty = false;
			},
			'change': function() {
				that.isDirty = true;
			}
		});
	},
	getFullName: function() {
		return this.get('FirstName') + ' ' + this.get('LastName');
	}
});

// Contacts Collection
var ContactsCollection = Backbone.Collection.extend({
	url: 'api/data/collections/name/contacts',
	model: Contact,
	initialize: function() {
	}
});

// Contacts Table Row
var ContactsCollectionItem = Backbone.View.extend({
	tagName: 'tr',
	attributes: function() {
		var attrs = {
			'unid': this.model.get('@unid')
		};
		return attrs;
	},
	events: {
		'click #deleteCollectionContact': 'deleteContact',
		'click': 'openContact'
	},
	initialize: function(options) {
		this.model = options.model;
	},
	render: function(evt) {
		var tmpl = _.template($('#ContactsTableItemTemplate').html());
		this.$el.html(tmpl(this.model.attributes));
	},
	openContact: function(evt) {
		var unid = this.$el.attr('unid');
		// Don't open the contact if the delete icon is clicked
		if (evt.target.tagName.toLowerCase() === 'td') {
			router.navigate('contacts/' + unid, {trigger: true});
		}
	},
	deleteContact: function(evt) {
		evt.preventDefault();
		var that = this;
		this.model.destroy({
			wait: true,
			dataType: 'text',
			success: function(model, response, options) {
				that.$el.fadeOut();
			},
			error: function(model, response, options) {
				console.error('destroy failed!, args=',arguments);
			}
		});
	}
});

// Contacts Table
var ContactsTableView = Backbone.View.extend({
	tagName: 'div',
	className: 'panel panel-primary',
	initialize: function(options) {
		this.collection = new ContactsCollection();
	},
	render: function() {
		var that = this;
		var tmpl = _.template($('#ContactsTableTemplate').html());
		this.$el.html(tmpl);
		$('#siteContent').html(this.el);
		this.collection.fetch({
			data: {
				count: 100
			},
			success: function(response, status, xhr) {
				_.each(response.models, function (contactItem, idx, models) {
					var contactsCollectionItem = new ContactsCollectionItem({model: contactItem});
					contactsCollectionItem.render();
					that.$el.find('tbody').append(contactsCollectionItem.el);
				});
			}
		});
	}
});

// Contact Form
var ContactView = Backbone.View.extend({
	tagName: 'div',
	className: 'panel panel-primary',
	events: {
		'click #deleteContact': 'deleteContact',
		'click #saveContact': 'saveContact',
		'click #closeContact': 'closeContact',
		'keyup input.form-control': 'updateModel'
	},
	initialize: function(options) {
		var that = this;
		if (options['@unid'] && options['@unid'] !== 'new') {
			this.model = new Contact({'@unid': options['@unid']});
			this.model.fetch({
				success: function(model, response, options) {
					that.render();
				},
				error: function(model, response, options) {
					console.error('Contact.fetch.error, args=',arguments);
				}
			});
		}else{
			this.model = new Contact();
		}
	},
	render: function() {
		var tmpl = _.template($('#ContactTemplate').html());
		this.$el.html(tmpl(this.model.attributes));
		$('#siteContent').html(this.el);
	},
	updateModel: function(evt) {
		var targetInput = $(evt.target);
		var fieldName = targetInput.attr('name');
		this.model.set(fieldName, targetInput.val());
	},
	saveContact: function(evt) {
		evt.preventDefault();
		var that = this;
		this.model.save(null, {
			success: function(model, response, options) {
				console.log("success, args=",arguments);
			},
			error: function(model, response, options) {
				if (response.status === 201) {
					var location = options.xhr.getResponseHeader('Location');
					var newUnid = location.substring(location.lastIndexOf('/'));
					router.navigate('#/contacts' + newUnid);
				}
			}
		});
	},
	deleteContact: function(evt) {
		evt.preventDefault();
		var that = this;
		var confirmDel = confirm("Really Delete the Contact for " + this.model.getFullName());
		if (confirmDel) {
			this.model.destroy({
				wait: true,
				dataType: 'text',
				success: function(model, response, options) {
					that.$el.fadeOut('slow', function() {
						router.navigate('#/contacts', {trigger: true});
					});
				}
			});
		}
	},
	closeContact: function(evt) {
		evt.preventDefault();
		if (this.model.isDirty) {
			var reallyClose = confirm('You have made changes. Closing will discard your changes. Proceed?');
			if (reallyClose) {
				router.navigate('#/contacts', {trigger: true});
			}
		}else{
			router.navigate('#/contacts', {trigger: true});
		}
	}
});

// The Router
var App = Backbone.Router.extend({
	routes: {
		'': 'home',
		'contacts': 'contacts',
		'contacts/:id': 'contacts',
		'contacts/:new': 'contacts'
	},
	home: function() {
		var welcome = '<div class=\'well text-center\'><h2>Welcome to the Contacts Backbone Application</h2></div>';
		$('#siteContent').html(welcome);
		this.updateActiveNav('Home');
	},
	contacts: function(contactId) {
		if (!contactId) {
			var contactsTableView = new ContactsTableView();
			contactsTableView.render();
		}else{
			var contactView = new ContactView({'@unid': contactId});
			if (contactId === 'new') {
				contactView.render();
			}
		}
		this.updateActiveNav('Contacts');
	},
	updateActiveNav: function(page) {
		var menuItems = $('.navbar .nav').children();
		menuItems.removeClass('active');
		menuItems.each(function (idx, menuItem) {
			var linkText = $(menuItem).find('a').text();
			if (linkText.indexOf(page) > -1) {
				$(menuItem).addClass('active');
			}
		});
	}
});

