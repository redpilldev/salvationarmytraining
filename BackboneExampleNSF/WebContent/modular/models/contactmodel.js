define([
	'backbone'
], function(Backbone) {

	// Contact Model
	var Contact = Backbone.Model.extend({
		idAttribute: '@unid',
		url: function() {
			if (this.isNew()) {
				return '../api/data/documents?Form=contact';
			}else{
				return Backbone.Model.prototype.url.apply(this);
			}
		},
		isDirty: false,
		urlRoot: '../api/data/documents/unid',
		defaults: {
			FirstName: '',
			LastName: '',
			Email: '',
			HomePhone: '',
			CellPhone: '',
			Supervisor: ''
		},
		initialize: function() {
			var that = this;
			this.on({
				'request': function() {
					that.isDirty = false;
				},
				'sync': function() {
					that.isDirty = false;
				},
				'change': function() {
					that.isDirty = true;
				}
			});
		},
		getFullName: function() {
			return this.get('FirstName') + ' ' + this.get('LastName');
		}
	});

	return Contact;

});
