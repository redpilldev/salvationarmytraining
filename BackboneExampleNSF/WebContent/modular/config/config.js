require.config({
	baseUrl: '../',
    shim: {
        jquery: {
            exports: '$'
        },
        bootstrap: {
            deps: ['jquery']
        },
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ['jquery', 'underscore'],
            exports: 'Backbone'
        }
    },
    paths: {
        underscore: 'lib/underscore',
        backbone: 'lib/backbone',
        jquery: 'lib/jquery',
        bootstrap: 'lib/bootstrap',
        text: 'lib/text',
        application: 'modular/router'
    },
    text: {
        env: 'xhr'
    }
});

require([
    'application',
    'bootstrap'
], function(App, Bootstrap) {
        router = new App();
        Backbone.history.start();
    }
);