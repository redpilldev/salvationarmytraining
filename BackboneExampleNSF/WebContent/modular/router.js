define([
	'backbone',
	'views/contactstableview.js',
	'views/contactview.js'
], function(Backbone, ContactsTableView, ContactView) {

	// The Router
	var App = Backbone.Router.extend({
		routes: {
			'': 'home',
			'contacts': 'contacts',
			'contacts/:id': 'contacts',
			'contacts/:new': 'contacts'
		},
		home: function() {
			var welcome = '<div class=\'well text-center\'><h2>Welcome to the Contacts Modular Backbone Application</h2></div>';
			$('#siteContent').html(welcome);
			this.updateActiveNav('Home');
		},
		contacts: function(contactId) {
			if (!contactId) {
				var contactsTableView = new ContactsTableView();
				contactsTableView.render();
			}else{
				var contactView = new ContactView({'@unid': contactId});
				if (contactId === 'new') {
					contactView.render();
				}
			}
			this.updateActiveNav('Contacts');
		},
		updateActiveNav: function(page) {
			var menuItems = $('.navbar .nav').children();
			menuItems.removeClass('active');
			menuItems.each(function (idx, menuItem) {
				var linkText = $(menuItem).find('a').text();
				if (linkText.indexOf(page) > -1) {
					$(menuItem).addClass('active');
				}
			});
		}
	});

	return App;

});
