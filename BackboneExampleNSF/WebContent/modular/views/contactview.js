define([
	'backbone',
	'models/contactmodel.js',
	'views/formessageview.js',
	'text!modular/templates/ContactForm.html'
], function(Backbone, Contact, FormMsgView, ContactFormHtml) {
	
	// Contact Form
	var ContactView = Backbone.View.extend({
		tagName: 'div',
		className: 'panel panel-primary',
		events: {
			'click #deleteContact': 'deleteContact',
			'click #saveContact': 'saveContact',
			'click #closeContact': 'closeContact',
			'keyup input.form-control': 'updateModel'
		},
		initialize: function(options) {
			var that = this;
			if (options['@unid'] && options['@unid'] !== 'new') {
				this.model = new Contact({'@unid': options['@unid']});
				this.model.fetch({
					success: function(model, response, options) {
						that.render();
					},
					error: function(model, response, options) {
						console.error('Contact.fetch.error, args=',arguments);
					}
				});
			}else{
				this.model = new Contact();
			}
		},
		render: function() {
			var tmpl = _.template(ContactFormHtml);
			this.$el.html(tmpl(this.model.attributes));
			$('#siteContent').html(this.el);
		},
		updateModel: function(evt) {
			var targetInput = $(evt.target);
			var fieldName = targetInput.attr('name');
			this.model.set(fieldName, targetInput.val());
		},
		saveContact: function(evt) {
			evt.preventDefault();
			var that = this;
			this.model.save(null, {
				success: function(model, response, options) {
					console.log("success, args=",arguments);
				},
				error: function(model, response, options) {
					var msgOptions = null;
					if (response.status === 201) {
						var location = options.xhr.getResponseHeader('Location');
						var newUnid = location.substring(location.lastIndexOf('/'));
						router.navigate('#/contacts' + newUnid);
					}else if (response.status >= 200 && response.status < 300) {
						msgOptions = {
							styleClass: 'alert-success',
							message: 'Contact Successfully Saved!'
						};
						var successMsgView = new FormMsgView(msgOptions).render();
					}else if (response.status > 300) {
						msgOptions = {
							styleClass: 'alert-danger',
							message: 'Contact NOT Saved! Error: ' + response.status
						};
						var failMsgView = new FormMsgView(msgOptions).render();
					}
				}
			});
		},
		deleteContact: function(evt) {
			evt.preventDefault();
			var that = this;
			var confirmDel = confirm("Really Delete the Contact for " + this.model.getFullName());
			if (confirmDel) {
				this.model.destroy({
					wait: true,
					dataType: 'text',
					success: function(model, response, options) {
						that.$el.fadeOut('slow', function() {
							router.navigate('#/contacts', {trigger: true});
						});
					}
				});
			}
		},
		closeContact: function(evt) {
			evt.preventDefault();
			if (this.model.isDirty) {
				var reallyClose = confirm('You have made changes. Closing will discard your changes. Proceed?');
				if (reallyClose) {
					router.navigate('#/contacts', {trigger: true});
				}
			}else{
				router.navigate('#/contacts', {trigger: true});
			}
		}
	});

	return ContactView;

});
