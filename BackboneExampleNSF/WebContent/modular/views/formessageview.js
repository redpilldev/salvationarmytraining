define([
	'backbone',
	'text!modular/templates/FormMsg.html'
], function(Backbone, FormMsgHtml) {

	var FormMsgView = Backbone.View.extend({
		tagName: 'div',
		template: _.template(FormMsgHtml, this.model),
		initialize: function(options) {
			this.model = new Backbone.Model(options);
		},
		render: function() {
			var tmpl = this.template(this.model.attributes);
			this.$el.html(tmpl);
			var panelBody = $('.panel-body');
			panelBody.prepend(this.el);

			// Show the message
			var msg = this.$el.find('#formAlert');
			msg.fadeIn();
			
			// Remove this view and message after 10 seconds
			var that = this;
			setInterval(function() {
				msg.fadeOut({
					complete: function() {
						that.remove();
					}
				});
			},10000);
		}
	});

	return FormMsgView;

});
