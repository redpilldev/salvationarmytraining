define([
	'backbone',
	'models/contactmodel.js',
	'text!modular/templates/ContactsTableItem.html'
], function(Backbone, Contact, ContactsTableItemHtml) {

	// Contacts Table Row
	var ContactsCollectionItem = Backbone.View.extend({
		tagName: 'tr',
		attributes: function() {
			var attrs = {
				'unid': this.model.get('@unid')
			};
			return attrs;
		},
		events: {
			'click #deleteCollectionContact': 'deleteContact',
			'click': 'openContact'
		},
		initialize: function(options) {
			this.model = options.model;
		},
		render: function(evt) {
			var tmpl = _.template(ContactsTableItemHtml);
			this.$el.html(tmpl(this.model.attributes));
		},
		openContact: function(evt) {
			var unid = this.$el.attr('unid');
			// Don't open the contact if the delete icon is clicked
			if (evt.target.tagName.toLowerCase() === 'td') {
				router.navigate('contacts/' + unid, {trigger: true});
			}
		},
		deleteContact: function(evt) {
			evt.preventDefault();
			var that = this;
			var confirmDel = confirm("Really Delete the Contact for " + this.model.getFullName());
			if (confirmDel) {
				this.model.destroy({
					wait: true,
					dataType: 'text',
					success: function(model, response, options) {
						that.$el.fadeOut();
					},
					error: function(model, response, options) {
						console.error('destroy failed!, args=',arguments);
					}
				});
			}
		}
	});

	return ContactsCollectionItem;

});
