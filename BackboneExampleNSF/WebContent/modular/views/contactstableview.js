define([
	'backbone',
	'views/contactstableitem.js',
	'collections/contactscollection.js',
	'text!modular/templates/ContactsTable.html'
], function(Backbone, ContactsCollectionItem, ContactsCollection, ContactsTableHtml) {

	var ContactsTableView = Backbone.View.extend({
		tagName: 'div',
		className: 'panel panel-primary',
		initialize: function(options) {
			this.collection = new ContactsCollection();
		},
		render: function() {
			var that = this;
			var tmpl = _.template(ContactsTableHtml);
			this.$el.html(tmpl);
			$('#siteContent').html(this.el);
			this.collection.fetch({
				data: {
					count: 100
				},
				success: function(response, status, xhr) {
					_.each(response.models, function (contactItem, idx, models) {
						var contactsCollectionItem = new ContactsCollectionItem({model: contactItem});
						contactsCollectionItem.render();
						that.$el.find('tbody').append(contactsCollectionItem.el);
					});
				}
			});
		}
	});

	return ContactsTableView;

});