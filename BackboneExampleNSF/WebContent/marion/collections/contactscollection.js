define([
	'backbone',
	'models/contactmodel.js'
], function(Backbone, Contact) {

	// Contacts Collection
	var ContactsCollection = Backbone.Collection.extend({
		url: '../api/data/collections/name/contacts',
		model: Contact,
		initialize: function() {
		}
	});

	return ContactsCollection;

});
