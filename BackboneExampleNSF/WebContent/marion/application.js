define([
	'marionette',
	'settings',
	'router.js',
	'views/layout.js'
], function(Marionette, Settings, Router, LayoutView) {

	var ContactApp = Backbone.Marionette.Application.extend({
		initialize: function(options) {
		
		},
		getLayoutRegion: function(regionName) {
			return this.layoutView.getRegion(regionName);
		},
		setupAjaxComplete: function() {
			$(document).bind('ajaxComplete', function(evt, xhr, xhrOptions){
				var response = xhr.responseText.toLowerCase();
				var authUrl = Settings.AUTHENTICATION_URL.toLowerCase();
				if (response.indexOf('action="' + authUrl + '"') > -1) {
					location.href = authUrl + '&redirectto=' + location.href;
				}
			});
		},
		onBeforeStart: function() {
			//this.setupAjaxPrefilter();
			this.setupAjaxComplete();
			this.layoutView = new LayoutView();
			$('body').prepend(this.layoutView.render().el);
		},
		onStart: function() {
			this.router = new Router();
			Backbone.history.start();
		}
	});

	return ContactApp;

});
