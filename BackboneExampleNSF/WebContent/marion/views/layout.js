define([
	'marionette',
	'text!marion/templates/Layout.html'
], function(Marionette, LayoutHtml) {

	var LayoutView = Backbone.Marionette.LayoutView.extend({
		template: _.template(LayoutHtml),
		regions: {
			CONTENT: '#siteContent'
		},
		initialize: function(options) {
		
		}
	});

	return LayoutView;

});
