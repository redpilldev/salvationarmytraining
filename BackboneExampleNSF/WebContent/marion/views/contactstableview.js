define([
	'marionette',
	'views/contactstableitem.js',
	'collections/contactscollection.js',
	'text!marion/templates/ContactsTable.html'
], function(Marionette, ContactsCollectionItem, ContactsCollection, ContactsTableHtml) {

	var ContactsTableView = Backbone.Marionette.CompositeView.extend({
		tagName: 'div',
		className: 'panel panel-primary',
		template: _.template(ContactsTableHtml, this.model),
		childView: ContactsCollectionItem,
		childViewContainer: 'tbody',
		initialize: function(options) {
			this.collection = new ContactsCollection();
			this.collection.fetch({
				data: {
					count: 100
				}
			});
		}
	});

	return ContactsTableView;

});