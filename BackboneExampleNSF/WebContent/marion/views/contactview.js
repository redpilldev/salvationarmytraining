define([
	'marionette',
	'models/contactmodel.js',
	'views/formessageview.js',
	'text!marion/templates/ContactForm.html'
], function(Marionette, Contact, FormMsgView, ContactFormHtml) {
	
	// Contact Form
	var ContactView = Backbone.Marionette.ItemView.extend({
		tagName: 'div',
		className: 'panel panel-primary',
		template: _.template(ContactFormHtml, this.model),
		events: {
			'click #deleteContact': 'deleteContact',
			'click #saveContact': 'saveContact',
			'click #closeContact': 'closeContact',
			'keyup input.form-control': 'updateModel'
		},
		initialize: function(options) {
			var that = this;
			if (options['@unid'] && options['@unid'] !== 'new') {
				this.model = new Contact({'@unid': options['@unid']});
				this.model.fetch({
					success: function(model, response, options) {
						that.render();
					},
					error: function(model, response, options) {
						console.error('Contact.fetch.error, args=',arguments);
					}
				});
			}else{
				this.model = new Contact();
			}
		},
		updateModel: function(evt) {
			var targetInput = $(evt.target);
			var fieldName = targetInput.attr('name');
			this.model.set(fieldName, targetInput.val());
		},
		saveContact: function(evt) {
			evt.preventDefault();
			var that = this;
			this.model.save(null, {
				success: function(model, response, options) {
					console.log("success, args=",arguments);
				},
				error: function(model, response, options) {
					var msgOptions = null;
					if (response.status === 201) {
						var location = options.xhr.getResponseHeader('Location');
						var newUnid = location.substring(location.lastIndexOf('/'));
						ContactApp.router.navigate('#/contacts' + newUnid);
					}else if (response.status >= 200 && response.status < 400) {
						msgOptions = {
							styleClass: 'alert-success',
							message: 'Contact Successfully Saved!'
						};
						var successMsgView = new FormMsgView(msgOptions).render();
					}else if (response.status >= 400) {
						msgOptions = {
							styleClass: 'alert-danger',
							message: 'Contact NOT Saved! Error: ' + response.status
						};
						var failMsgView = new FormMsgView(msgOptions).render();
					}
				}
			});
		},
		deleteContact: function(evt) {
			evt.preventDefault();
			var that = this;
			var confirmDel = confirm("Really Delete the Contact for " + this.model.getFullName());
			if (confirmDel) {
				this.model.destroy({
					wait: true,
					dataType: 'text',
					success: function(model, response, options) {
						that.$el.fadeOut('slow', function() {
							ContactApp.router.navigate('#/contacts', {trigger: true});
						});
					}
				});
			}
		},
		closeContact: function(evt) {
			evt.preventDefault();
			if (this.model.isDirty) {
				var reallyClose = confirm('You have made changes. Closing will discard your changes. Proceed?');
				if (reallyClose) {
					ContactApp.router.navigate('#/contacts', {trigger: true});
				}
			}else{
				ContactApp.router.navigate('#/contacts', {trigger: true});
			}
		}
	});

	return ContactView;

});
