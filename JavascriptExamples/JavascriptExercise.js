/**
 * These snippets are meant to be copy/pasted into the javascript console
 * during the course of the Javascript class.
 */

/*****************Start Variables******************/
var notGlobal = function() {
	var myVar = 'foo';
};
myGlobalVar = 'foo';
console.log('myGlobalVar = ',window.myGlobalVar);
console.log('myVar = ',window.myVar);
/*****************End Variables******************/

/*****************Start Data Types******************/
// Example 1
var x = 16 + 'foo';
console.log('16 + \'foo\' = ',x);

// Example 2
var x = 8 + 8 + 'foo';
console.log('8 + 8 + \'foo\' = ',x);

// Example 3
var x = 'foo' + 8 + 8;
console.log('\'foo\' + 8 + 8 = ',x);

// Example 4
var x = 'foo';
console.log('x is a typeof ', typeof x);
/*****************End Data Types******************/

/*****************Start Functions******************/
function myFunction() {
	// Do something
}
var myFunctionVar = function(arg1, arg2, arg3, arg4) {
	console.log('myFunctionVar arguments=',arguments);
	arg4();
};
myFunction();
myFunctionVar('foo', 42, {baz: 'baz'}, function() {console.log('I\'m a callback');});
/*****************End Functions******************/

/*****************Start Comparators******************/
// Exercise 1
var foo = "1";
var Foo = 1;
if (foo == Foo) {
	console.log("1 The Same");
}else{
	console.log("1 Not the Same");
}

// Exercise 2
if (foo = Foo) {
	console.log("2 The Same, foo is a " + typeof foo);
}else{
	console.log("2 Not the Same, foo is a " + typeof foo);
}

// Exercise 3
if (foo === Foo) {
	console.log("3 The Same");
}else{
	console.log("3 Not the Same");
}

// Exercise 4
if (foo = null) {
	console.log("4 The Same, foo is a " + typeof foo);
}else{
	console.log("4 Not the Same, foo is a " + typeof foo);
}

/*****************End Comparators******************/

/*****************Start Truthy/Falsey******************/
var yes = 'yes';
var nullVal = null;
if (yes) {
	console.log('yes is truthy');
}else{
	console.log('yes is falsey');
}
if (!nullVal) {
	console.log('nullVal is falsey');
}else{
	console.log('nullVal is truthy');
}
/*****************End Truthy/Falsey******************/

/*****************Start Objects******************/
var myObj = {
	foo: 'bar',
	bar: function() {
		return this.foo;
	}
};
console.log('myObj.foo = ',myObj.foo);
console.log('myObj["foo"] = ',myObj['foo']);
console.log('myObj.bar returns ',myObj.bar());
/*****************End Objects******************/

/*****************Start Closure******************/
var MyAPI = function() {
	var myPrivateVar = 'foo';
	var myapi = {
		getMyPrivateVar: function() {
			return myPrivateVar;
		},
		setMyPrivateVar: function(foo) {
			myPrivateVar = foo;
		}
	};
	return myapi;
}();
var temp = MyApi.getMyPrivateVar();
MyApi.setMyPrivateVar("bar");
/*****************End Closure******************/

/*****************Start Syntax******************/
/* BAD Syntax */
var fooVar = 'foo'
var barVar = 'barVar'
if (fooVar)
console.log(fooVar)
else
console.log(barVar)
console.log('done')

/* GOOD Syntax */
var fooVar = 'foo';
var barVar = 'barVar';
if (fooVar) {
	console.log(fooVar);
}else{
	console.log(barVar);
}
console.log('done');
/*****************End Syntax******************/

/*****************Start Meaningful Whitespace******************/
// 1 Meaningful Whitespace
var FOO = (function() 
{
    return 
    {      
        init: function()
        {
            console.log("init running");
        }
    }
})();
FOO.init();

// 2 No Meaningful Whitespace
// NOTICE: jsHint is still complaining about missing semi-colons
var FOO = (function()
{
    return {
        init: function()
        {
            console.log("init running");
        }
    }
})();
FOO.init();

// 3 How Javascript Interprets the first example
var FOO = (function()
{
    return //<---- A ";" will be added here
    {      
        init: function()
        {
            console.log("init running");
        }
    }
})();
FOO.init();

// 4 How it should be written
// NOTICE: jsHint no longer complains
var FOO = (function() {
    return {
        init: function()
        {
            console.log("init running");
        }
    };
})();
FOO.init();
/*****************End Meaningful Whitespace******************/

/*****************Start 'this'******************/
function doSomething() {
	console.log('doSomething, this=', this);
	console.log('*****************');
}
function doContextualCallback(callback) {
	console.log('doContextualCallback, this=',this);
	callback();
	console.log('*****************');
}
var myObj = {
	doSomething: function() {
		console.log('myObj.doSomething, this=', this);
		console.log('*****************');
	},
	doCallback: function(callback) {
		console.log('myObj.doCallback, this=',this);
		callback();
		console.log('*****************');
	},
	doContextCallback: function(callback) {
		console.log('myObj.doContextCallback, this=',this);
		doContextualCallback.call(this, callback);
		console.log('*****************');
	},
	doContextPassing: function(callback) {
		console.log('myObj.doContextPassing, this=',this);
		callback.call(this);
		console.log('*****************');
	}
};

doSomething();
myObj.doSomething();

// Do some callbacks passing around this
var ourCallback = function() {
	console.log('ourCallback function, this=',this);
};
myObj.doCallback(ourCallback);
myObj.doContextCallback(ourCallback);
myObj.doContextPassing(ourCallback);
/*****************End 'this'******************/

/*****************Start Loops******************/
var myArray = ['foo', 'bar', 'baz'];
var someObj = {
	foo: 'foo',
	bar: 'bar',
	baz: 'baz'
};

// for loop
for(var i = 0; i < myArray.length; i++) {
	console.log('value of myArray[' + i + '] is ', myArray[i]);
}
console.log('*****************');

// for/in loop
for(var key in someObj) {
	console.log('value of someObj[\'' + someObj[key] + '\'] is ', someObj[key]);
}
console.log('*****************');

// while loop
var count = 0;
while(count < myArray.length) {
	console.log('value of myArray[' + count + '] is ', myArray[count]);
	count++;
}
console.log('*****************');

// do/while loop
var count = 0;
do {
	console.log('value of myArray[' + count + '] is ', myArray[count]);
	count++;
}
while(count < myArray.length);
console.log('*****************');
/*****************End Loops******************/
